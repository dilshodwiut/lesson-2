// task 1. divisible by 3 and 5
function divisibleBy3And5(num) {
  if (typeof num == "number") {
    return num % 3 == 0 && num % 5 == 0;
  } else {
    console.log("Not a valid number");
  }
}

// task 2. num is even or odd
function isOdd(num) {
  if (typeof num == "number") {
    return num % 2 != 0;
  } else {
    console.log("Not a valid number");
  }
}

// task 3. sort array in ascending order

function sortArr(arr) {
  arr.sort(function (a, b) {
    return b < a;
  });
  return arr;
}

// task 4. a unique set of array
function getUniqueItems(arr) {
  var uniqueItems = [];
  for (let item of arr) {
    for (let key in item) {
      uniqueItems = uniqueItems.concat(item[key]);
    }
  }
  uniqueItems = uniqueItems.flat();
  return new Set(uniqueItems);
}

// task 5. compare objects by keys and values, shallow comparison, naive solution
function isSame(obj1, obj2) {
  if (Object.keys(obj1).length != Object.keys(obj2).length) {
    return false;
  }

  if (Object.keys(obj1).length == 0) {
    return true;
  }

  for (let key in obj1) {
    return obj1[key] === obj2[key];
  }
}
